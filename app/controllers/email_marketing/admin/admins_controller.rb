module EmailMarketing
  module Admin
    class AdminsController < ::Admin::AdminsController
      helper EmailMarketing::ApplicationHelper

      def marketing_connection
        ::MarketingConnection::Base.new(api_key: ::MarketingConnection.api_key, api_secret: ::MarketingConnection.api_secret)
      end

      def set_errors(object, errors, overides = {})
        errors.each do |k,v|
          new_k = overides[k.to_sym]
          k = new_k if new_k
          object.errors.add(k.to_sym, v.first)
        end
      end

    end
  end
end
