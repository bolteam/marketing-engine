module EmailMarketing
  module Admin
    class MarketingListsController < AdminsController

      def index
        response = marketing_connection.lists.list
        @lists = response["lists"]
      end

      def show
        response = marketing_connection.lists.find(params[:id])
        @emails = response["emails"]
      end

      def new
        @list = List.new
      end

      def create
        got_params = params.require(:admin_list).permit(:name, :list_emails)
        @list = List.new(got_params)
#        if @list.valid?
          response = @list.submit
          errors = response["errors"]
          if errors
            set_errors(@list, errors)
            render "new"
          else
            redirect_to admin_marketing_lists_path, flash: {success: "Lista creada exitosamente"}
          end
#        else
#          render "new"
#        end
      end

      def new_csv
        @list = List.new
      end


      def create_csv
        got_params = params.require(:admin_list).permit(:name, :csv_file)
        @list = List.new(got_params)
        response = @list.submit_csv
        errors = response["errors"]
        if errors
          set_errors(@list, errors)
          render "new_csv"
        else
          redirect_to admin_marketing_lists_path, flash: {success: response["notification"]}
        end
      end



    end


    class List < AdminsController
      include ActiveModel::Model
      attr_accessor :name, :list_emails, :csv_file

      #validates :name, presence: true
      #validates :list_emails, presence: true

      def submit
        json = JSON(self.to_json)
        parameters =  {list: json}
        response = marketing_connection.lists.create(parameters)
      end

      def submit_csv
        attrs = {
          name: self.name,
          csv_file: self.csv_file
        }
        response = marketing_connection.lists.create_csv(list: attrs)
      end




    end

  end
end
