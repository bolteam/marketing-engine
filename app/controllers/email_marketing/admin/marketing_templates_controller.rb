module EmailMarketing
  module Admin
    class MarketingTemplatesController < AdminsController
      layout :get_layout
      def index
        response = marketing_connection.templates.list
        @templates = response["templates"]
      end

      def show
        response = marketing_connection.templates.find(params[:id])
        @template = response
      end


      private
        def get_layout
          "blank" if action_name == "show"
        end

    end
  end
end
