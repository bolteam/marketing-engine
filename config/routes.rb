EmailMarketing::Engine.routes.draw do

  namespace :admin do
    resources :marketing_templates, only: [:index, :show]
    resources :marketing_lists, except: :destroy do
      get "new_csv", to: "marketing_lists#new_csv", as: :new_csv, on: :collection
      post "create_csv", to: "marketing_lists#create_csv", as: :create_csv, on: :collection
    end
    resources :marketing_deliveries, only: [:index, :new, :create]
  end

end
