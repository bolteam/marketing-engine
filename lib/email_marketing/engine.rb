module EmailMarketing
  class Engine < ::Rails::Engine
    isolate_namespace EmailMarketing
    require "mailgun"
    require "marketing_connection"
  end
end
